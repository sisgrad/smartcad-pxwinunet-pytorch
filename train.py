import logging
import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
import sys

import torch
import torch.nn as nn
from torch import optim


from core.NN import PxWinUnet
from core.nn_ops.loss import DiceBCELoss

from utils.dataloader import BasicDataset
from torch.utils.data import DataLoader, random_split

from utils.eval import eval_dice
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

import neptune.new as neptune
from neptune.new.types import File

def initialize_weights(m):
  if isinstance(m, nn.Conv2d):
      nn.init.kaiming_uniform_(m.weight.data)
      if m.bias is not None:
          nn.init.constant_(m.bias.data, 0)
  if isinstance(m, nn.ConvTranspose2d):
      nn.init.kaiming_uniform_(m.weight.data)
      if m.bias is not None:
          nn.init.constant_(m.bias.data, 0)
  elif isinstance(m, nn.BatchNorm2d):
      nn.init.constant_(m.weight.data, 1)
      nn.init.constant_(m.bias.data, 0)

dir_img = 'data/imgs/'
dir_mask = 'data/masks/'
dir_boundaries = 'data/boundaries/'
dir_checkpoint = 'checkpoints/'


"""
G E N E R A L    T R A I N   H P A R A M S:
"""
epochs = 201
batch_size = 1
lr = 0.001
val_percent = 0.1
img_scale = 0.5

"""
T R A N S F O R M E R    H P A R A M S:
"""

"""
Feature map is splitted into patches
each patch contains patch_size*patch_size pixels
attention is calculated between pixels (patch=window)
default: 2
must be >=2
"""
patch_size = 2

"""
Number of succesive Window Transformer blocks at each stage,
default: [2, 2, 2]
"""
num_blocks = [2, 2, 2]

"""
Embedding dimension of Window Transformer at each stage
default: [256, 512, 1024]
"""
embed_dim = [256, 512, 1024]

"""
Resolution of Window Transformer input feature map from conv layers
Resolution of conv feature maps:
h2 = H/2*W/2  
h3 = H/4*W/4
h4 = H/8*W/4

input_resolution for Window Transformer:
same as conv if patch_size=2
otherwise:
h2 = H/patch_size*W/patch_size
h3 = H/2/patch_size*W/2/patch_size
h4 = H/4/patch_size*W/4/patch_size

[240, 120, 60] are good enough for default patch size=2 (Tesla P100 16 Gb)
"""
input_resolution = [240, 120, 60]

"""
Number of heads in multi-head attention,
default: 16
"""
num_heads = 16

"""
Other Window Transformer hparams,
default:
mlp_ratio = 4
qkv_bias = True
qk_scale = None
attn_dropout = 0.
proj_dropout = 0.
drop_path = 0.
activation = nn.ReLU
norm_layer = nn.LayerNorm
"""
mlp_ratio = 4
qkv_bias = True
qk_scale = None
attn_dropout = 0.
proj_dropout = 0.
drop_path = 0.
activation = nn.ReLU
norm_layer = nn.LayerNorm


loss_fn = DiceBCELoss()

# NEPTUNE.AI TRACKING
hparams_dict = {'epochs': epochs, 'batch_size': batch_size, 'learning_rate': lr,
                'val_percent': val_percent, 'img_scale': img_scale, 'patch_size': patch_size,
                'num_blocks': num_blocks, 'embed_dim': embed_dim,
                'input_resolution': input_resolution, 'num_heads': num_heads,
                'mlp_ratio:': mlp_ratio, 'qkv_bias': qkv_bias, 'qk_scale': qk_scale,
                'attn_dropout': attn_dropout, 'proj_dropout': proj_dropout,
                'drop_path': drop_path, 'activation': activation, 'norm_layer': norm_layer}

optimizer = optim.AdamW



def train_net(net,
              device,
              epochs,
              batch_size,
              lr,
              val_percent,
              img_scale,
              save_cp=True):

    dataset = BasicDataset(dir_img, dir_mask, dir_boundaries, img_scale)
    n_val = int(len(dataset) * val_percent)
    n_train = len(dataset) - n_val
    train, val = random_split(dataset, [n_train, n_val])
    train_loader = DataLoader(train, batch_size=batch_size, shuffle=True, num_workers=2, pin_memory=True)
    val_loader = DataLoader(val, batch_size=batch_size, shuffle=False, num_workers=2, pin_memory=True, drop_last=True)

    writer = SummaryWriter(comment=f'LR_{lr}_BS_{batch_size}_SCALE_{img_scale}')
    global_step = 0

    logging.info(f'''Starting training:
        Epochs:          {epochs}
        Batch size:      {batch_size}
        Learning rate:   {lr}
        Training size:   {n_train}
        Validation size: {n_val}
        Checkpoints:     {save_cp}
        Device:          {device.type}
        Images scaling:  {img_scale}
    ''')

    optim = optimizer(net.parameters(), lr=lr)



    for epoch in range(epochs):
        net.train()

        epoch_loss = 0
        with tqdm(total=n_train, desc=f'Epoch {epoch + 1}/{epochs}', unit='img') as pbar:
            for batch in train_loader:
                imgs = batch['transformed_image']
                true_masks = batch['transformed_mask']
                true_boundaries = batch['transformed_boundary']


                imgs = imgs.to(device=device, dtype=torch.float32)
                mask_type = torch.float32
                boundary_type = torch.float32
                true_masks = true_masks.to(device=device, dtype=mask_type)
                true_boundaries = true_boundaries.to(device=device, dtype=boundary_type)

                boundary_first_order_preds, mask_first_order_preds, boundary_preds, mask_preds = net(imgs)

                loss1 = loss_fn(boundary_first_order_preds, true_boundaries)
                loss2 = loss_fn(mask_first_order_preds, true_masks)
                loss3 = loss_fn(boundary_preds, true_boundaries)
                loss4 = loss_fn(mask_preds, true_masks)
                loss = loss1 + loss2 + loss3 + loss4

                epoch_loss += loss.item()
                writer.add_scalar('Loss/train', loss.item(), global_step)

                pbar.set_postfix(**{'loss (batch)': loss.item()})

                optim.zero_grad()
                loss.backward()
                #nn.utils.clip_grad_value_(net.parameters(), 0.1)
                optim.step()

                pbar.update(imgs.shape[0])
                global_step += 1

                """
                T E N S O R B O A R D   V I S U A L I Z A T I O N:
                """

                mask_score, boundary_score = eval_dice(net, val_loader, device)

                logging.info('/n')
                logging.info('STEP: {}'.format(global_step))
                logging.info('MASK DICE EVAL: {}'.format(mask_score))
                logging.info('BOUNDARIES DICE EVAL: {}'.format(boundary_score))

                # NEPTUNE.AI TRACKING
                # Log batch loss
                run["training/batch/loss"].log(loss)
                # Log batch dice
                run["training/batch/mask_dice"].log(mask_score)
                run["training/batch/boundary_dice"].log(boundary_score)

                writer.add_scalar('Dice/mask_test', mask_score, global_step)
                writer.add_scalar('Dice/boundary_test', boundary_score, global_step)

                #writer.add_images('images', imgs, global_step)
                #writer.add_images('masks/true', true_masks, global_step)
                writer.add_images('masks/pred', torch.sigmoid(mask_preds) > 0.5, global_step)
                writer.add_images('boundaries/pred', torch.sigmoid(boundary_preds) > 0.5, global_step)

        if save_cp:
            try:
                os.mkdir(dir_checkpoint)
                logging.info('Created checkpoint directory')
            except OSError:
                pass
            saving_instances = [30, 60, 90, 120, 150, 160, 165, 170, 175, 177, 180, 182, 185, 187, 190, 192, 195, 197, 199, 200]

            for e in saving_instances:
              if e == epoch:
                torch.save(net.state_dict(), dir_checkpoint + f'CP_epoch{epoch}.pth')
                logging.info(f'Checkpoint {epoch} saved !')


    writer.close()

if __name__ == '__main__':
    run = neptune.init(
        project="vilgefortz2300/smartcad-PYTORCH",
        api_token="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vYXBwLm5lcHR1bmUuYWkiLCJhcGlfdXJsIjoiaHR0cHM6Ly9hcHAubmVwdHVuZS5haSIsImFwaV9rZXkiOiJkODRmMDMxYS1jMjZjLTQ0MzgtYTcwNS1jNzc5NzkzYzgzODEifQ==",
    )  # your credentials

    run['config/criterion'] = loss_fn
    run['config/optimizer'] = optimizer
    run['config/params'] = hparams_dict  # dict() object
    run['source_code/files'].upload_files("core/*.py")

    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    logging.info(f'Using device {device}')

    net = PxWinUnet(num_blocks=num_blocks, embed_dim=embed_dim,
                        patch_size=patch_size, input_resolution=input_resolution, num_heads=num_heads,
                            mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                                qk_scale=qk_scale, attn_dropout=attn_dropout,
                                    proj_dropout=proj_dropout, drop_path=drop_path,
                                        activation=activation, norm_layer=norm_layer)
    net.to(device=device)
    net.apply(initialize_weights)

    try:
        train_net(net=net,
                  epochs=epochs,
                  batch_size=batch_size,
                  lr=lr,
                  device=device,
                  img_scale=img_scale,
                  val_percent=val_percent)
    except KeyboardInterrupt:
        torch.save(net.state_dict(), 'INTERRUPTED.pth')
        logging.info('Saved interrupt')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)