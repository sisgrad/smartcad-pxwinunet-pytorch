# SmartCAD-PxWinUnet-PyTorch

CNN+Window Attention Transformer for archeology drawings automation.

![alt text](https://gitlab.com/sisgrad/smartcad-pxwinunet-pytorch/-/raw/main/examples.png)

## Used methods

### General:

- Boundary-aware U-net(implemented from scratch) [paper](https://www.mdpi.com/1424-8220/21/8/2615/htm)
- Swin Transformer(code, ideas) [repo](https://github.com/microsoft/Swin-Transformer)/[paper](https://arxiv.org/pdf/2103.14030v2.pdf)
- Swin-Unet(ideas) [paper](https://arxiv.org/pdf/2105.05537v1.pdf)
- TransUnet(ideas) [paper](https://arxiv.org/pdf/2102.04306v1.pdf)

### Additional:

- Weight Standartization [code](https://github.com/joe-siyuan-qiao/WeightStandardization)/[paper](https://arxiv.org/pdf/1903.10520v2.pdf)
- Group Normalization [paper](https://arxiv.org/pdf/1803.08494v3.pdf)

## Proposed method:

Full NN scheme in scheme/scheme.dwg (AutoCAD drawing)

Boundary-aware U-net is used as baseline
It's encoder is made of truncated ResNet18. Window Transformer blocks are processing encoder's shortcuts.

It's decoder is built from several blocks: 
- 2 full-sized decoder branches(mask_subnetwork_1, boundary_subnetwork_1)
- boundary-mask fusion block
- 2 mini-sized decoder branches(mask_-subnetwork_2, boundary_subnetwork_2)

#### During training:
- No-pre trained weights are used.
- Encoder is initialized randomly with kaiming distribution.
- Window Transformer blocks initialized with truncated normal distribution (as in original repo).

#### In encoder:
- C1 is passed directly to decoder.
- Feature maps from stages C2, C3, C4 are upsampled to C1, C2, C3 stages spatial-sizes respectively.
- Upsampled shortcuts are processed in x2 transformer blocks each.
- Processed by transformer shortcuts are downsampled back to C2, C3, C4 stages spatial-sizes respectively 
and passed to decoder as C2, C3, C4 shortcuts.
- Original, not processed by transformer C4 are passed further to deeper encoder layers.

#### In decoder:
- Each full-sized decoder branch outputs mask or boundaries by conv1x1, loss_1 and loss_2 are computed respectively.
- Hidden units before conv1x1 from full-sized decoder branches are summed element-wise and passed to boundary-mask fusion block (BMFB).
- Each mini-sized decoder branch of BMFB outputs mask or boundaries, loss_3 and loss_4 are computed respectively.

During prediction mask and boundary outputs of BMFB mini-sized decoder branches are used for testing and further processing to dxf drawing.

```
cd existing_repo
git remote add origin https://gitlab.com/sisgrad/smartcad-pxwinunet-pytorch.git
git branch -M main
git push -uf origin main
```

## Visuals

## Installation

## Usage

## Roadmap

## Project status

