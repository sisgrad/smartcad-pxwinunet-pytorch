from PIL import Image
import  cv2
from matplotlib import  pyplot as plt
import numpy as np


#img = Image.open("test_predict/0003.png").convert('L')

#Image._show(img)

#img_raw = cv2.imread("0030.png", flags=cv2.IMREAD_GRAYSCALE)
img_raw = cv2.imread("data/imgs/0001.png", flags=cv2.IMREAD_GRAYSCALE)
#img_raw = cv2.imread("orthoPhoto1280.jpg", flags=cv2.IMREAD_GRAYSCALE)

preprocessed_img = cv2.bilateralFilter(img_raw, 14, 20, 20)

# Contrast Limiting Adaptive Histogram Equalization (CLAHE)
#clahe = cv2.createCLAHE(clipLimit =100.0, tileGridSize=(10,10))

#preprocessed_img = clahe.apply(img_raw)

#preprocessed_img = cv2.adaptiveThreshold(preprocessed_img,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
            #cv2.THRESH_BINARY,11,2)

cv2.imwrite('postproc_img.png', preprocessed_img)

pil_preprocessed_img = Image.fromarray(preprocessed_img)

Image._show(pil_preprocessed_img)

# img = Image.open('test_predict/0001.png').convert('L')
#
# img_cv = np.array(img)
#
# bilated = cv2.bilateralFilter(img_cv, 5, 42, 42)
#
# cv2.imshow('result', bilated)
#
# cv2.waitKey(0)