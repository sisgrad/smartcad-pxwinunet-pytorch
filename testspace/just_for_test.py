import torch
from torch.nn import  Conv2d, BatchNorm2d
from torch.nn.functional import relu

# class ResidualBlock(torch.nn.Module):
#     #Residual block of ResNet
#     def __init__(self, in_channels, out_channels, kernel, stride=(1,1)):
#         super(ResidualBlock, self).__init__()
#         self.conv1 = Conv2d(in_channels, out_channels, kernel_size=kernel, stride=stride, padding=(1,1))
#         self.bn1 = BatchNorm2d(out_channels)
#         self.conv2 = Conv2d(out_channels, out_channels, kernel_size=kernel, stride=(1,1), padding=(1,1))
#         self.bn2 = BatchNorm2d(out_channels)
#         self.fcconv1 = Conv2d(in_channels, out_channels, kernel_size=(1,1), stride=(2,2))
#     def forward(self, input_tensor, downsample=False):
#         x = relu(self.bn1(self.conv1(input_tensor)))
#         x = self.bn2(self.conv2(x))
#         if downsample is True:
#             input_tensor = self.fcconv1(input_tensor)
#         x += input_tensor
#         return relu(x)
#
# data = torch.randn((1,1,256,256))
# blk1 = ResidualBlock(1, 64, (3,3), stride=2)
# blk2 = ResidualBlock(64, 64, (3,3))
#
# x = blk1(data, downsample=True)
# x = blk2(x)
#
# print(x.shape)

x = torch.randn((1,1,256,256))

print(x.size())
print(x.size()[2])

n_sample_1 = int(len(dataset) * 0.2)
n_sample_2 = len(dataset) - n_sample_1
sampled_dataset_1, sampled_dataset_2 = random_split(dataset, [n_sample_1, n_sample_2],
                                                    generator=torch.Generator().manual_seed(seed))
n_val = int(len(sampled_dataset_1) * val_percent)
n_train = len(sampled_dataset_1) - n_val
train, val = random_split(sampled_dataset_1, [n_train, n_val], generator=torch.Generator().manual_seed(seed))
train_loader = DataLoader(train, batch_size=batch_size, shuffle=True, num_workers=2, pin_memory=True)
val_loader = DataLoader(val, batch_size=batch_size, shuffle=False, num_workers=2, pin_memory=True, drop_last=True)