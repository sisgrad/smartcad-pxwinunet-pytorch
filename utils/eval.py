import torch
from tqdm import tqdm


def dice_coeff(input, target):
    """Dice coeff for individual examples"""
    eps = 0.0001
    intersection = torch.dot(input.view(-1), target.view(-1))
    union = torch.sum(input) + torch.sum(target) + eps

    t = (2 * intersection.float() + eps) / union.float()
    return t


def batch_dice_coeff(input, target):
    """Dice coeff for batches"""
    if input.is_cuda:
        s = torch.FloatTensor(1).cuda().zero_()
    else:
        s = torch.FloatTensor(1).zero_()

    for i, c in enumerate(zip(input, target)):
        s = s + dice_coeff(c[0], c[1])

    return s / (i + 1)

def eval_dice(net, loader, device):
    """Evaluation without the densecrf with the dice coefficient"""
    net.eval()
    mask_type = torch.float32
    boundary_type = torch.float32
    n_val = len(loader)  # the number of batch
    mask_tot = 0
    boundary_tot = 0

    with tqdm(total=n_val, desc='Validation round', unit='batch', leave=False) as pbar:
        for batch in loader:
            imgs, true_masks, true_boundaries = batch['transformed_image'], batch['transformed_mask'], batch['transformed_boundary']
            imgs = imgs.to(device=device, dtype=torch.float32)
            true_masks = true_masks.to(device=device, dtype=mask_type)
            true_boundaries = true_boundaries.to(device=device, dtype=boundary_type)

            with torch.no_grad():
                boundary_first_order_preds, mask_first_order_preds, boundary_preds, mask_preds = net(imgs)


            mask_preds = (torch.sigmoid(mask_preds) > 0.5).float()

            boundary_preds = (torch.sigmoid(boundary_preds) > 0.5).float()

            mask_tot += batch_dice_coeff(mask_preds, true_masks).item()

            boundary_tot += batch_dice_coeff(boundary_preds, true_boundaries).item()

        pbar.update()

    net.train()
    return mask_tot / n_val, boundary_tot / n_val
