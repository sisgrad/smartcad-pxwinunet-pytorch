from os.path import splitext
from os import listdir
import numpy as np
from glob import glob
import torch
from torch.utils.data import Dataset
import logging
from PIL import Image
import albumentations as A
import cv2
from torchvision import transforms

crop = transforms.CenterCrop(960)

transform = A.Compose([
    A.VerticalFlip(p=0.5),
    A.HorizontalFlip(p=0.5)
])

transform_img_with_clahe = A.Compose([
    A.FancyPCA(p=0.5),
    A.CLAHE(clip_limit=100.0, tile_grid_size=(10,10), p=0.5),
    A.HueSaturationValue(p=0.5)
])

class BasicDataset(Dataset):
    def __init__(self, imgs_dir, masks_dir, boundaries_dir, scale=1, mask_suffix='', boundary_suffix=''):
        self.imgs_dir = imgs_dir
        self.masks_dir = masks_dir
        self.boundaries_dir = boundaries_dir
        self.scale = scale
        self.mask_suffix = mask_suffix
        self.boundary_suffix = boundary_suffix
        assert 0 < scale <= 1, 'Scale must be between 0 and 1'

        self.ids = [splitext(file)[0] for file in listdir(imgs_dir)
                    if not file.startswith('.')]
        logging.info(f'Creating dataset with {len(self.ids)} examples')

    def __len__(self):
        return len(self.ids)

    @classmethod
    def preprocess_labels(cls, pil_img, scale):
        w, h = pil_img.size
        newW, newH = int(scale * w), int(scale * h)
        assert newW > 0 and newH > 0, 'Scale is too small'
        pil_img = pil_img.resize((newW, newH))

        img_nd = np.array(pil_img)

        if len(img_nd.shape) == 2:
            img_nd = np.expand_dims(img_nd, axis=2)
        # HWC to CHW
        img_trans = img_nd.transpose((2, 0, 1))
        if img_trans.max() > 1:
            img_trans = img_trans / 255
        #norm = np.zeros((960,960))
        #final = cv2.normalize(img_trans,  norm, 0, 1, cv2.NORM_MINMAX)
        return img_trans


    @classmethod
    def preprocess_imgs(cls, pil_img, scale):
        w, h = pil_img.size
        newW, newH = int(scale * w), int(scale * h)
        assert newW > 0 and newH > 0, 'Scale is too small'
        pil_img = pil_img.resize((newW, newH))

        img_nd = np.array(pil_img)

        # Preprocessing from paper "An Improved Boundary-Aware U-Net for Ore Image Semantic Segmentation"
        preprocessed_img = cv2.bilateralFilter(img_nd, 14, 14, 14)

        transformed_with_clahe = transform_img_with_clahe(image=preprocessed_img)

        # Contrast Limiting Adaptive Histogram Equalization (CLAHE)
        #clahe = cv2.createCLAHE(clipLimit=100.0, tileGridSize=(10, 10))

        #preprocessed_img = clahe.apply(img_nd)   # preprocessing implementation completed

        preprocessed_img = cv2.cvtColor(preprocessed_img, cv2.COLOR_RGB2GRAY)

        if len(preprocessed_img.shape) == 2:
            preprocessed_img = np.expand_dims(preprocessed_img, axis=2)

        # HWC to CHW
        img_trans = preprocessed_img.transpose((2, 0, 1))
        if img_trans.max() > 1:
            img_trans = img_trans / 255
        #norm = np.zeros((960,960))
        #final = cv2.normalize(img_trans,  norm, 0, 1, cv2.NORM_MINMAX)
        return img_trans

    @classmethod
    def preprocess_to_inference(cls, pil_img):

        img_nd = np.array(pil_img)

        # Preprocessing from paper "An Improved Boundary-Aware U-Net for Ore Image Semantic Segmentation"
        preprocessed_img = cv2.bilateralFilter(img_nd, 14, 14, 14)

        # Contrast Limiting Adaptive Histogram Equalization (CLAHE)
        # clahe = cv2.createCLAHE(clipLimit=100.0, tileGridSize=(10, 10))

        # preprocessed_img = clahe.apply(img_nd)   # preprocessing implementation completed

        preprocessed_img = cv2.cvtColor(preprocessed_img, cv2.COLOR_RGB2GRAY)

        if len(preprocessed_img.shape) == 2:
            preprocessed_img = np.expand_dims(preprocessed_img, axis=2)

        # HWC to CHW
        img_trans = preprocessed_img.transpose((2, 0, 1))
        if img_trans.max() > 1:
            img_trans = img_trans / 255
        # norm = np.zeros((960,960))
        # final = cv2.normalize(img_trans,  norm, 0, 1, cv2.NORM_MINMAX)
        return img_trans

    def __getitem__(self, i):
        idx = self.ids[i]
        img_file = glob(self.imgs_dir + idx + '.*')
        mask_file = glob(self.masks_dir + idx + self.mask_suffix + '.*')
        boundary_file = glob(self.boundaries_dir + idx + self.boundary_suffix + '.*')

        assert len(img_file) == 1, \
            f'Either no image or multiple images found for the ID {idx}: {img_file}'
        assert len(mask_file) == 1, \
            f'Either no mask or multiple masks found for the ID {idx}: {mask_file}'
        assert len(boundary_file) == 1, \
            f'Either no mask or multiple masks found for the ID {idx}: {boundary_file}'
        # Read img and mask in rgb mode
        img = Image.open(img_file[0])
        #mask = Image.open(mask_file[0])
        #boundary = Image.open(boundary_file[0])

        # Read img and mask in grayscale mode
        #img = Image.open(img_file[0]).convert('L')
        mask = Image.open(mask_file[0]).convert('L')
        boundary = Image.open(boundary_file[0]).convert('L')

        img = crop(img)
        mask = crop(mask)
        boundary = crop(boundary)

        assert img.size == mask.size == boundary.size, \
            f'Image and mask {idx} should be the same size, but are {img.size} / {mask.size} / {boundary.size}'

        img = self.preprocess_imgs(img, self.scale)
        mask = self.preprocess_labels(mask, self.scale)
        boundary = self.preprocess_labels(boundary, self.scale)

        transformed = transform(image=img, mask=mask, boundary=boundary)
        transformed_image = transformed['image']
        transformed_mask = transformed['mask']
        transformed_boundary = transformed['boundary']

        return {
            'transformed_image': torch.from_numpy(img).type(torch.FloatTensor),
            'transformed_mask': torch.from_numpy(mask).type(torch.FloatTensor),
            'transformed_boundary': torch.from_numpy(boundary).type(torch.FloatTensor)
        }