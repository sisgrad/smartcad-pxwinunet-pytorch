import argparse
import logging
import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from torchvision import transforms
import cv2

from core.NN import PxWinUnet
from utils.dataloader import BasicDataset
from torch.nn.functional import relu

input_output = [1280, 960, 480]

"""
params of trained model:
"""
patch_size = 2

num_blocks = [2, 2, 2]

embed_dim = [256, 512, 1024]

input_resolution = [240, 120, 60]

num_heads = 16

mlp_ratio = 4
qkv_bias = True
qk_scale = None
attn_dropout = 0.
proj_dropout = 0.
drop_path = 0.
activation = nn.ReLU
norm_layer = nn.LayerNorm

def predict_img(net,
                full_img,
                device,
                out_threshold=0.5):
    net.eval()

    pre_tf = transforms.Compose(
        [   transforms.CenterCrop(input_output[1]),
            transforms.Resize((input_output[2], input_output[2]))
        ]
    )
    img = pre_tf(full_img)
    img = torch.from_numpy(BasicDataset.preprocess_to_inference(img))

    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        boundary_first_order_preds, mask_first_order_preds, boundary_preds, mask_preds = net(img)

        #probs = torch.sigmoid(mask_preds) - torch.sigmoid(boundary_preds)

        probs = torch.sigmoid(mask_preds)

        probs = probs.squeeze(0)

        tf = transforms.Compose(
            [
                transforms.ToPILImage(),
                transforms.Resize(input_output[1]),
                transforms.ToTensor()
            ]
        )

        probs = tf(probs.cpu())
        full_mask = probs.squeeze().cpu().numpy()

        full_mask = cv2.copyMakeBorder(full_mask, top=0, bottom=0, left=160, right=160, borderType=cv2.BORDER_CONSTANT)

    return full_mask > out_threshold


def get_args():
    parser = argparse.ArgumentParser(description='Predict masks from input images',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--model', '-m', default='MODEL.pth',
                        metavar='FILE',
                        help="Specify the file in which the model is stored")
    parser.add_argument('--input', '-i', metavar='INPUT', nargs='+',
                        help='filenames of input images', required=True)

    parser.add_argument('--output', '-o', metavar='INPUT', nargs='+',
                        help='Filenames of ouput images')
    parser.add_argument('--mask-threshold', '-t', type=float,
                        help="Minimum probability value to consider a mask pixel white",
                        default=0.5)

    return parser.parse_args()


def get_output_filenames(args):
    in_files = args.input
    out_files = []

    if not args.output:
        for f in in_files:
            pathsplit = os.path.splitext(f)
            out_files.append("{}_OUT{}".format(pathsplit[0], pathsplit[1]))
    elif len(in_files) != len(args.output):
        logging.error("Input files and output files are not of the same length")
        raise SystemExit()
    else:
        out_files = args.output

    return out_files


def mask_to_image(mask):
    return Image.fromarray((mask * 255).astype(np.uint8))


if __name__ == "__main__":
    args = get_args()
    in_files = args.input
    out_files = get_output_filenames(args)

    net = PxWinUnet(num_blocks=num_blocks, embed_dim=embed_dim,
                        patch_size=patch_size, input_resolution=input_resolution, num_heads=num_heads,
                            mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                                qk_scale=qk_scale, attn_dropout=attn_dropout,
                                    proj_dropout=proj_dropout, drop_path=drop_path,
                                        activation=activation, norm_layer=norm_layer)

    logging.info("Loading model {}".format(args.model))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    logging.info(f'Using device {device}')
    net.to(device=device)
    net.load_state_dict(torch.load(args.model, map_location=device))

    logging.info("Model loaded !")

    for i, fn in enumerate(in_files):
        logging.info("\nPredicting image {} ...".format(fn))

        #img = Image.open(fn).convert('L')

        img = Image.open(fn)

        mask = predict_img(net=net,
                           full_img=img,
                           out_threshold=args.mask_threshold,
                           device=device)

        out_fn = out_files[i]
        result = mask_to_image(mask)
        result.save(out_files[i])

        logging.info("Mask saved to {}".format(out_files[i]))
