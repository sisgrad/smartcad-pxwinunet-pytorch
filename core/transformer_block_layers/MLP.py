import torch.nn as nn


class MLP(nn.Module):
    def __init__(self, in_features, hidden_features, activation=nn.GELU, proj_dropout=0.):
        super().__init__()
        self.fc1 = nn.Linear(in_features, hidden_features)
        self.act = activation()
        self.fc2 = nn.Linear(hidden_features, in_features)
        self.drop = nn.Dropout(proj_dropout)

    def forward(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x
