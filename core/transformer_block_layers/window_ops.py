from einops import rearrange


def einops_window_partition(x, window_size):
    """
    :param x: batch of images with size of B,H,W,C
    :param window_size: size of patch (Wh, Ww)
    :num_patches: Num_h*Num_w
    :return: B*num_patches, Wh, Ww, C
    """
    windows = rearrange(x, 'b (Num_h Wh) (Num_w Ww) c -> (b Num_h Num_w) Wh Ww c',
                        Wh=window_size, Ww=window_size)
    return windows

def einops_window_reverse(windows, window_size, H, W):
    """
    :param windows: batch of windows with size of B*num_patches, Wh, Ww, C
    :param window_size: size of patch (Wh, Ww)
    :param H: H of original image
    :param W: W of original image
    var num_patches: Num_h*Num_w
    :return: batch of images with size of B,H,W,C
    """
    reversed_windows = rearrange(windows, '(b Num_h Num_w) Wh Ww c -> b (Num_h Wh) (Num_w Ww) c ',
                                 Num_h=H//window_size, Num_w=W//window_size, Wh=window_size, Ww=window_size)
    return reversed_windows
