import  torch
from core.encoder import Encoder
from core.decoder import DecoderHead, BMFB

class PxWinUnet(torch.nn.Module):
    #Paper implementation "An Improved Boundary-Aware U-Net for Ore Image Semantic Segmentation"
    def __init__(self, num_blocks, embed_dim, patch_size, input_resolution, num_heads,
                                mlp_ratio, qkv_bias, qk_scale, attn_dropout, proj_dropout, drop_path, activation, norm_layer):
        super().__init__()
        self.encoder = Encoder(num_blocks, embed_dim, patch_size, input_resolution, num_heads,
                               mlp_ratio, qkv_bias, qk_scale, attn_dropout, proj_dropout, drop_path, activation, norm_layer)

        self.boundary_subnetwork = DecoderHead()
        self.mask_subnetwork = DecoderHead()
        self.fusion_block = BMFB()

    def forward(self, train_data):
        h1, h2, h3, h4, x = self.encoder(train_data)
        hidden_var_boundary, boundary_first_order_preds = self.boundary_subnetwork(h1, h2, h3, h4, x)
        hidden_var_mask, mask_first_order_preds = self.mask_subnetwork(h1, h2, h3, h4, x)
        boundary_pred, mask_pred = self.fusion_block(hidden_var_boundary, hidden_var_mask)
        return (boundary_first_order_preds, mask_first_order_preds, boundary_pred, mask_pred)