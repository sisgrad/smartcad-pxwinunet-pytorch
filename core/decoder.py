import  torch
from torch.nn import Conv2d, BatchNorm2d, ConvTranspose2d
from torch.nn.functional import mish
from core.FCN_blocks import SubnetworkBlock, LogitsBlock


class DecoderHead(torch.nn.Module):
    #Receiving inputs from encoder (h1, h2, h3, h4, x)
    #two decoder heads return preds for loss1 and loss2 and 2xhidden_var for further processing in BMFB
    def __init__(self):
        super(DecoderHead, self).__init__()
        self.transconv = ConvTranspose2d(1024, 1024, (2,2), stride=(2,2))
        self.trans_bn = BatchNorm2d(1024)
        self.blk1 = SubnetworkBlock((1536, 1024, 512), (1024,512,512), (3,3))
        self.blk2 = SubnetworkBlock((768, 512, 256), (512,256,256), (3,3))
        self.blk3 = SubnetworkBlock((384,256,128), (256,128,128), (3,3))
        self.conv1 = Conv2d(192, 128, (3,3), padding=(1,1))
        self.bn1 = BatchNorm2d(128)
        self.conv2 = Conv2d(128, 64, (3,3), padding=(1,1))
        self.bn2 = BatchNorm2d(64)
        self.conv3 = Conv2d(64, 64, (3,3), padding=(1,1))
        self.bn3 = BatchNorm2d(64)
        self.boundout1 = Conv2d(64, 1, kernel_size=(1,1))
        self.bn4 = BatchNorm2d(1)
    def forward(self, h1, h2, h3, h4, x):
        x = mish(self.trans_bn(self.transconv(x)), inplace=True)
        x = torch.cat([x, h4], dim=1)
        x = self.blk1(x)
        x = torch.cat([x, h3], dim=1)
        x = self.blk2(x)
        x = torch.cat([x, h2], dim=1)
        x = self.blk3(x)
        x = torch.cat([x, h1], dim=1)
        x = mish(self.bn1(self.conv1(x)), inplace=True)
        x = mish(self.bn2(self.conv2(x)), inplace=True)
        hidden_var = mish(self.bn3(self.conv3(x)), inplace=True)
        first_order_pred = mish(self.bn4(self.boundout1(hidden_var)), inplace=True)
        return hidden_var, first_order_pred

class BMFB(torch.nn.Module):
    #Boundary-mask fusion block (returns loss3 and loss4)
    def __init__(self):
        super(BMFB, self).__init__()
        self.conv1 = Conv2d(128, 128, (3,3), padding=(1,1))
        self.bn1 = BatchNorm2d(128)
        self.conv2 = Conv2d(128, 64, (3,3), padding=(1,1))
        self.bn2 = BatchNorm2d(64)
        self.conv3 = Conv2d(64, 64, (3,3), padding=(1,1))
        self.bn3 = BatchNorm2d(64)
        self.boundout2 = LogitsBlock()
        self.maskout2 = LogitsBlock()
    def forward(self, h5, hidden_var):
        fused = torch.cat([h5,hidden_var], dim=1)
        x = mish(self.bn1(self.conv1(fused)), inplace=True)
        x = mish(self.bn2(self.conv2(x)), inplace=True)
        x = mish(self.bn3(self.conv3(x)), inplace=True)
        boundary_pred = self.boundout2(x)
        mask_pred = self.maskout2(x)
        return (boundary_pred, mask_pred)