import torch.nn as nn
from core.transformer_block_layers.WMSA import WMSA
from core.transformer_block_layers.MLP import MLP
from core.nn_ops.dropout_ops import DropPath


class WindowTransformerBlock(nn.Module):
    r""" Window Transformer Block.
    Args:
        embed_dim (int): Number of input channels.
        input_resolution (int): Input resolution.
        num_heads (int): Number of attention heads.
        mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
        qkv_bias (bool, optional): If True, add a learnable bias to query, key, value. Default: True
        qk_scale (float | None, optional): Override default qk scale of head_dim ** -0.5 if set.
        proj_dropout (float, optional): Dropout rate. Default: 0.0
        attn_dropout (float, optional): Attention dropout rate. Default: 0.0
        drop_path (float, optional): Stochastic depth rate. Default: 0.0
        activation (nn.Module, optional): Activation layer. Default: nn.GELU
        norm_layer (nn.Module, optional): Normalization layer.  Default: nn.LayerNorm
    """

    def __init__(self, embed_dim, input_resolution, num_heads,
                 mlp_ratio, qkv_bias, qk_scale, attn_dropout, proj_dropout, drop_path, activation, norm_layer):
        super().__init__()
        self.H_W = (input_resolution, input_resolution)

        self.norm1 = norm_layer(embed_dim)

        # Windowed multi-head self-attention
        self.attn = WMSA(
            embed_dim=embed_dim, input_resolution=input_resolution, num_heads=num_heads,
            qkv_bias=qkv_bias, qk_scale=qk_scale,
            attn_dropout=attn_dropout, proj_dropout=proj_dropout)

        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        self.norm2 = norm_layer(embed_dim)
        self.mlp = MLP(in_features=embed_dim, hidden_features=embed_dim*mlp_ratio, activation=activation, proj_dropout=proj_dropout)

    def forward(self, x):
        H, W = self.H_W
        B, L, C = x.shape
        assert L == H * W, "input feature has wrong size"

        # Residual connection
        shortcut = x

        x = self.norm1(x)
        x = x.view(B, H, W, C)

        # Windowed multi-head self-attention
        x = self.attn(x)

        # FFN
        x = shortcut + self.drop_path(x)
        x = x + self.drop_path(self.mlp(self.norm2(x)))

        return x

class WindowTransformer(nn.Module):
    def __init__(self, num_blocks, embed_dim, input_resolution, num_heads,
                mlp_ratio, qkv_bias, qk_scale,
                attn_dropout, proj_dropout, drop_path,
                activation, norm_layer):
        super().__init__()
        self.num_blocks = num_blocks
        self.embed_dim = embed_dim
        self.blocks = nn.ModuleList()
        for i in range(num_blocks):
            Window_Transformer_Block = WindowTransformerBlock(embed_dim=embed_dim,
                               input_resolution=input_resolution,
                               num_heads=num_heads,
                               mlp_ratio=mlp_ratio,
                               qkv_bias=qkv_bias,
                               qk_scale=qk_scale,
                               attn_dropout=attn_dropout,
                               proj_dropout=proj_dropout,
                               drop_path=drop_path,
                               activation=activation,
                               norm_layer=norm_layer
                               )
            self.blocks.append(Window_Transformer_Block)

        print(f'Num of Window Transformer blocks with dim {self.embed_dim}:', len(self.blocks))

    def forward(self, x):
        for block in self.blocks:
            x = block(x)
        return x
