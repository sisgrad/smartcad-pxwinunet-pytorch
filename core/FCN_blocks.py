import torch
from torch.nn import Conv2d, BatchNorm2d, ConvTranspose2d
from torch.nn.functional import mish, dropout


class ResidualBlock(torch.nn.Module):
    #Residual block of ResNet
    def __init__(self, in_channels, out_channels, kernel):
        super(ResidualBlock, self).__init__()
        self.conv1 = Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel, stride=(1,1), padding=(1,1))
        self.down_conv1 = Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel, stride=(2,2), padding=(1, 1))
        self.bn1 = BatchNorm2d(out_channels)
        self.conv2 = Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel, stride=(1,1), padding=(1,1))
        self.bn2 = BatchNorm2d(out_channels)
        self.down_fcconv1 = Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=(1,1), stride=(2,2))

    def forward(self, input_tensor, downsample=False):
        #Downsample flag is only used for 1, 3, 5, 7 blocks
        #due to need of matching dimensions of output with skip connection data
        if downsample is True:
            x = mish(self.bn1(self.down_conv1(input_tensor)), inplace=True)
            x = self.bn2(self.conv2(x))
            input_tensor = self.down_fcconv1(input_tensor)
        else:
            x = mish(self.bn1(self.conv1(input_tensor)), inplace=True)
            x = self.bn2(self.conv2(x))
        x += input_tensor
        return mish(x, inplace=True)


class SubnetworkBlock(torch.nn.Module):
    #Block to construct subnetworks
    def __init__(self, in_channels, out_channels, kernel):
        super().__init__()
        in1, in2, in3 = in_channels
        out1, out2, out3 = out_channels

        self.conv1 = Conv2d(in1, out1, kernel_size=kernel, stride=(1,1), padding=(1,1))
        self.bn1 = BatchNorm2d(out1)
        self.conv2 = Conv2d(in2, out2, kernel_size=kernel, stride=(1,1), padding=(1,1))
        self.bn2 = BatchNorm2d(out2)
        self.transconv = ConvTranspose2d(in3, out3, kernel_size=(2,2), stride=(2,2))
        self.bn3 = BatchNorm2d(out3)
    def forward(self, x):
        x = mish(self.bn1(self.conv1(x)), inplace=True)
        x = mish(self.bn2(self.conv2(x)), inplace=True)
        x = mish(self.bn3(self.transconv(x)), inplace=True)
        return x


class LogitsBlock(torch.nn.Module):
    #Outputs logits for 3,4 losses
    def __init__(self):
        super(LogitsBlock, self).__init__()
        self.conv1 = Conv2d(64, 64, (3,3), padding=(1,1))
        self.bn1 = BatchNorm2d(64)
        self.conv2 = Conv2d(64, 64, (3,3), padding=(1,1))
        self.bn2 = BatchNorm2d(64)
        self.outconv = Conv2d(64, 1, (1,1))
        self.bn3 = BatchNorm2d(1)
    def forward(self, h):
        h = mish(self.bn1(self.conv1(h)), inplace=True)
        h = mish(self.bn2(self.conv2(h)), inplace=True)
        logits = mish(self.bn3(self.outconv(h)), inplace=True)
        return logits