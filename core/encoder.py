import torch
from torch.nn import Conv2d, ConvTranspose2d, BatchNorm2d, MaxPool2d
from torch.nn.functional import mish
from core.FCN_blocks import ResidualBlock
from core.window_transformer import WindowTransformer
from einops import rearrange

class Encoder(torch.nn.Module):
    #Encoder based on truncated ResNet18, according to paper sensors-21-02615
    def __init__(self, num_blocks, embed_dim, patch_size, input_resolution, num_heads,
                 mlp_ratio, qkv_bias, qk_scale, attn_dropout, proj_dropout, drop_path,activation, norm_layer):
        super(Encoder, self).__init__()
        self.input_resolution = input_resolution

        self.conv1 = Conv2d(1, 64, (3,3), padding=(1,1))
        self.bn3 = BatchNorm2d(64)
        self.blk1 = ResidualBlock(64, 64, (3,3))
        self.blk1_2 = ResidualBlock(64, 64, (3,3))
        self.fcconv1 = Conv2d(64, 128, kernel_size=(1,1))
        self.blk2 = ResidualBlock(128, 128, (3,3))
        self.blk2_2 = ResidualBlock(128, 128, (3,3))
        self.in_transconv1 = ConvTranspose2d(128, 1, kernel_size=(2,2), stride=(2,2))
        self.window_conv1 = Conv2d(1, embed_dim[0], kernel_size=(patch_size, patch_size), stride=(patch_size, patch_size))
        self.transformer1 = WindowTransformer(num_blocks[0], embed_dim[0], input_resolution[0], num_heads,
                                                mlp_ratio, qkv_bias, qk_scale,
                                                attn_dropout, proj_dropout, drop_path,
                                                activation, norm_layer)
        self.out_transconv1 = ConvTranspose2d(embed_dim[0], 128, kernel_size=(patch_size, patch_size), stride=(patch_size, patch_size) )
        self.pool_trans1 = MaxPool2d(kernel_size=(2,2), stride=(2,2))

        self.fcconv2 = Conv2d(128, 256, kernel_size=(1,1))
        self.blk3 = ResidualBlock(256, 256, (3,3))
        self.blk3_2 = ResidualBlock(256, 256, (3,3))
        self.in_transconv2 = ConvTranspose2d(256, 1, kernel_size=(2,2), stride=(2,2))
        self.window_conv2 = Conv2d(1, embed_dim[1], kernel_size=(patch_size, patch_size), stride=(patch_size, patch_size))
        self.transformer2 = WindowTransformer(num_blocks[1], embed_dim[1], input_resolution[1], num_heads,
                                                mlp_ratio, qkv_bias, qk_scale,
                                                attn_dropout, proj_dropout, drop_path,
                                                activation, norm_layer)
        self.out_transconv2 = ConvTranspose2d(embed_dim[1], 256, kernel_size=(patch_size, patch_size), stride=(patch_size, patch_size) )
        self.pool_trans2 = MaxPool2d(kernel_size=(2, 2), stride=(2, 2))

        self.fcconv3 = Conv2d(256, 512, kernel_size=(1,1))
        self.blk4 = ResidualBlock(512, 512, (3,3))
        self.blk4_2 = ResidualBlock(512, 512, (3,3))
        self.in_transconv3 = ConvTranspose2d(512, 1, kernel_size=(2,2), stride=(2,2))
        self.window_conv3 = Conv2d(1, embed_dim[2], kernel_size=(patch_size, patch_size), stride=(patch_size, patch_size))
        self.transformer3 = WindowTransformer(num_blocks[2], embed_dim[2], input_resolution[2], num_heads,
                                                mlp_ratio, qkv_bias, qk_scale,
                                                attn_dropout, proj_dropout, drop_path,
                                                activation, norm_layer)
        self.out_transconv3 = ConvTranspose2d(embed_dim[2], 512, kernel_size=(patch_size, patch_size), stride=(patch_size, patch_size) )
        self.pool_trans3 = MaxPool2d(kernel_size=(2, 2), stride=(2, 2))

        self.pool1 = MaxPool2d(kernel_size=(2,2), stride=(2,2))
        self.conv2 = Conv2d(512, 1024, (3,3), padding=(1,1))
        self.bn1 = BatchNorm2d(1024)
        self.conv3 = Conv2d(1024, 1024, (3,3), padding=(1,1))
        self.bn2 = BatchNorm2d(1024)
    def forward(self, data):
        x = mish(self.bn3(self.conv1(data)), inplace=True)
        h1 = self.blk1_2(self.blk1(x))
        h2 = self.blk2_2(self.blk2(self.fcconv1(h1), downsample=True))

        h3 = self.blk3_2(self.blk3(self.fcconv2(h2), downsample=True))

        h4 = self.blk4_2(self.blk4(self.fcconv3(h3), downsample=True))

        transformer_thread_1 = self.window_conv1(self.in_transconv1(h2))
        transformer_thread_1 = rearrange(transformer_thread_1, 'B C H W -> B (H W) C')
        transformer_thread_1 = self.transformer1(transformer_thread_1)
        transformer_thread_1 = rearrange(transformer_thread_1, 'B (H W) C -> B C H W', H=self.input_resolution[0], W=self.input_resolution[0])
        transformer_thread_1 = self.out_transconv1(transformer_thread_1)


        transformer_thread_2 = self.window_conv2(self.in_transconv2(h3))
        transformer_thread_2 = rearrange(transformer_thread_2, 'B C H W -> B (H W) C')
        transformer_thread_2 = self.transformer2(transformer_thread_2)
        transformer_thread_2 = rearrange(transformer_thread_2, 'B (H W) C -> B C H W', H=self.input_resolution[1], W=self.input_resolution[1])
        transformer_thread_2 = self.out_transconv2(transformer_thread_2)


        transformer_thread_3 = self.window_conv3(self.in_transconv3(h4))
        transformer_thread_3 = rearrange(transformer_thread_3, 'B C H W -> B (H W) C')
        transformer_thread_3 = self.transformer3(transformer_thread_3)
        transformer_thread_3 = rearrange(transformer_thread_3, 'B (H W) C -> B C H W', H=self.input_resolution[2], W=self.input_resolution[2])
        transformer_thread_3 = self.out_transconv3(transformer_thread_3)

        h2 = h2 + self.pool_trans1(transformer_thread_1)

        h3 = h3 + self.pool_trans2(transformer_thread_2)

        h4 = h4 + self.pool_trans3(transformer_thread_3)


        x = mish(self.pool1(h4))
        x = mish(self.bn1(self.conv2(x)), inplace=True)
        x = mish(self.bn2(self.conv3(x)), inplace=True)
        return (h1, h2, h3, h4, x)