import argparse
import logging
import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from torchvision import transforms
import cv2

from core.NN import PxWinUnet
from utils.dataloader import BasicDataset
from torch.nn.functional import relu

input_output = [1280, 960, 480]

model = 'checkpoints/swinunetcolab_CP_epoch197.pth'
scale = 1
#img = 'testspace/DJI_0624.png'
#img = 'ablay.png'
img = 'testspace/0030.png'
#img = 'testspace/00w.png'
#img = '0026.png'
#img = '0031.png'
#img = 'data/imgs/0001.png'
#img = 'data/imgs/0021.png'
#img = 'data/imgs/0025.png'
mask_threshold = 0.5
output_filename = 'local_output.jpg'

"""
params of trained model:
"""
patch_size = 2

num_blocks = [2, 2, 2]

embed_dim = [256, 512, 1024]

input_resolution = [240, 120, 60]

num_heads = 16

mlp_ratio = 4
qkv_bias = True
qk_scale = None
attn_dropout = 0.
proj_dropout = 0.
drop_path = 0.
activation = nn.ReLU
norm_layer = nn.LayerNorm


def predict_img(net,
                full_img,
                device,
                out_threshold=0.5):
    net.eval()

    pre_tf = transforms.Compose(
        [   transforms.CenterCrop(input_output[1]),
            transforms.Resize((input_output[2], input_output[2]))
        ]
    )
    img = pre_tf(full_img)
    img = torch.from_numpy(BasicDataset.preprocess_to_inference(img))

    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        boundary_first_order_preds, mask_first_order_preds, boundary_preds, mask_preds = net(img)


        #probs = torch.sigmoid(mask_preds) - torch.sigmoid(boundary_preds)

        probs = torch.sigmoid(mask_preds)

        probs = probs.squeeze(0)

        tf = transforms.Compose(
            [
                transforms.ToPILImage(),
                transforms.Resize(input_output[1]),
                transforms.ToTensor()
            ]
        )


        probs = tf(probs.cpu())
        full_mask = probs.squeeze().cpu().numpy()

        full_mask = cv2.copyMakeBorder(full_mask, top=0, bottom=0, left=160, right=160, borderType=cv2.BORDER_CONSTANT)

    return full_mask > out_threshold

def get_output_filenames(output_filename):
    in_files = img
    out_files = []

    if not output_filename:
        for f in in_files:
            pathsplit = os.path.splitext(f)
            out_files.append("{}_OUT{}".format(pathsplit[0], pathsplit[1]))
    else:
        out_files = output_filename

    return out_files


def mask_to_image(mask):
    return Image.fromarray((mask * 255).astype(np.uint8))


if __name__ == "__main__":
    in_files = input
    out_file = output_filename

    net = PxWinUnet(num_blocks=num_blocks, embed_dim=embed_dim,
                        patch_size=patch_size, input_resolution=input_resolution, num_heads=num_heads,
                            mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                                qk_scale=qk_scale, attn_dropout=attn_dropout,
                                    proj_dropout=proj_dropout, drop_path=drop_path,
                                        activation=activation, norm_layer=norm_layer)

    logging.info("Loading model {}".format(model))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    logging.info(f'Using device {device}')
    net.to(device=device)
    net.load_state_dict(torch.load(model, map_location=device))

    logging.info("Model loaded !")

    logging.info("\nPredicting image {} ...".format(img))

    #img = Image.open(img).convert('L')

    img = Image.open(img)

    mask = predict_img(net=net,
                        full_img=img,
                        out_threshold=mask_threshold,
                        device=device)

    out_fn = out_file
    result = mask_to_image(mask)
    result.save(out_file)

    Image._show(result)
    logging.info("Mask saved to {}".format(out_file))
